#How to setup SoftEther VPN Server on Ubuntu 15.04

###Introduction

This article explains how to install and configure a multi-protocol VPN server using the SoftEther package.
We enable and configure OpenVPN and L2TP over IPSec and SSTP VPN Servers on Ubuntu Linux 15.04.

##What is SoftEther

SoftEther VPN is one of the world's most powerful and easy-to-use multi-protocol VPN software,
made by the good folks at the University of Tsukuba, Japan. It runs on Windows, Linux, Mac,
FreeBSD and Solaris and is freeware and open-source.
You can use SoftEther for any personal or commercial use free of charge.

#Step 1: Create a Virtual Server
